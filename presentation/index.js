// Import React
import React from "react";

// Import Spectacle Core tags
import {
  Appear,
  BlockQuote,
  Cite,
  CodePane,
  Deck,
  Fill,
  Heading,
  Image,
  Layout,
  Link,
  ListItem,
  List,
  Markdown,
  Quote,
  Slide,
  Spectacle,
  Text
} from "spectacle";

// Import image preloader util
import preloader from "spectacle/lib/utils/preloader";

// Import theme
import createTheme from "spectacle/lib/themes/default";

// Require CSS
require("normalize.css");
require("spectacle/lib/themes/default/index.css");


const images = {
  quantified_ialja: require("../assets/quantified_ialja.png"),
  motorica_render: require("../assets/motorica_render.jpg"),
  goto_logo: require("../assets/goto_logo.svg"),
};

preloader(images);

const theme = createTheme({
  primary: "#ff4081"
});

export default class Presentation extends React.Component {
  render() {
    return (
      <Spectacle theme={theme}>
        <Deck transition={["zoom", "slide"]} transitionDuration={500}>
          <Slide transition={["zoom"]} bgColor="primary">
            <Heading size={1} fit caps lineHeight={1}>
              <Link textColor="black" href="https://gitlab.com/prosthetic-metrics">Prosthetic metrics</Link>
            </Heading>
            <Heading size={1} fit caps>
              A subset of <Link textColor="white" href="https://github.com/graddfril">gräddfril</Link>
            </Heading>
            <Heading size={1} fit caps textColor="black">
              Where You Gather Arbitrary (-ish) Data and Analyze 'em
            </Heading>
          </Slide>
          <Slide transition={["slide"]} bgColor="black" notes="You can even put notes on your slide. How awesome is that?">
            <Image src={images.quantified_ialja.replace("/", "")} margin="0px auto 40px" height="500px"/>
            <Heading size={2} caps fit textColor="primary" textFont="primary">
              The Quantified Self
            </Heading>
          </Slide>
          <Slide transition={["slide"]} bgColor="black">
            <Image src="https://pixabay.com/static/uploads/photo/2015/05/25/05/27/network-782707_960_720.png" margin="0px auto 40px" height="500px"/>
            <Heading size={2} caps fit textColor="primary" textFont="primary">
              Internet of Things
            </Heading>
          </Slide>
          <Slide transition={["slide"]} bgColor="black">
            <Heading size={1} caps textColor="primary" textFont="primary">
              The Problem
            </Heading>
          </Slide>
          <Slide transition={["slide"]} bgColor="black">
            <Heading size={1} caps textColor="primary" textFont="primary">
              The Problem²
            </Heading>
          </Slide>
          <Slide transition={["slide"]} bgImage={images.motorica_render.replace("/", "")} bgDarken={0.75}>
            <Appear fid="1">
              <Heading size={1} caps fit textColor="primary">
                Functional wrist prosthetics
              </Heading>
            </Appear>
            <Appear fid="2">
              <Heading size={1} caps fit textColor="tertiary">
                A concrete case
              </Heading>
            </Appear>
            <Appear fid="3">
              <Heading size={1} caps fit textColor="primary">
                Small-scale production
              </Heading>
            </Appear>
          </Slide>
          <Slide transition={["zoom", "fade"]} bgColor="primary">
            <Heading caps fit>Data Flow</Heading>
            <Layout>
              <Fill>
                <Heading size={4} caps textColor="secondary" bgColor="white" margin={10}>
                  Prosthetic
                </Heading>
              </Fill>
	      <Heading size={4} caps textColor="secondary">
	        ➔
	      </Heading>
              <Fill>
                <Heading size={4} caps textColor="secondary" bgColor="white" margin={10}>
                  Phone
                </Heading>
              </Fill>
	      <Heading size={4} caps textColor="secondary">
	        ➔
	      </Heading>
              <Fill>
                <Heading size={4} caps textColor="secondary" bgColor="white" margin={10}>
                  Server
                </Heading>
              </Fill>
            </Layout>
          </Slide>
          <Slide transition={["fade"]} bgColor="primary">
	    <Heading caps>Done</Heading>
            <List>
              <Appear><ListItem>Hardware testing device (SPP)</ListItem></Appear>
              <Appear><ListItem>Android app (Java; SPP/JSON over HTTP)</ListItem></Appear>
            </List>
          </Slide>
          <Slide transition={["fade"]} bgColor="primary">
	    <Heading caps>To Be Done</Heading>
            <List>
              <Appear><ListItem>Server-ish</ListItem></Appear>
	      <List>
	        <Appear><ListItem>Matrix for raw events</ListItem></Appear>
		<Appear><ListItem>??? for parsed events storage</ListItem></Appear>
		<Appear><ListItem>??? for aggregation</ListItem></Appear>
              </List>
	      <Appear><ListItem>Better hardware</ListItem></Appear>
	      <List>
	        <Appear><ListItem>Interrupt all the things!</ListItem></Appear>
		<Appear><ListItem>Production-ish MCUs</ListItem></Appear>
		<Appear><ListItem>Proper board</ListItem></Appear>
		<Appear><ListItem>BLE</ListItem></Appear>
	      </List>
            </List>
          </Slide>
          <Slide transition={["spin", "slide"]} bgColor="tertiary">
            <Heading size={1} caps fit lineHeight={1.5} textColor="primary">
              Prototype made at the GoTo camp
            </Heading>
            <Link href="https://github.com/goto-ru"><Image width="100%" src={images.goto_logo}/></Link>
          </Slide>
        </Deck>
      </Spectacle>
    );
  }
}
