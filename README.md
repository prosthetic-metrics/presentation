# Prosthetic metrics presentation @ [GoTo](https://github.com/goto-ru) Camp June '16

Made with [spectacle](https://github.com/FormidableLabs/spectacle/), and is basically a light adaption of their [boilerplate](https://github.com/FormidableLabs/spectacle-boilerplate/). Shiny!

## How to run

```sh
npm install # get deps
npm start # start up a local webserver
xdg-open localhost:3000 # open up a browser
```
